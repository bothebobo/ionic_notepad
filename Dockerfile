FROM beevelop/ionic:latest

RUN mkdir /opt/notepad && \
    bash -c "cd /opt/notepad && ionic start ionic-notepad blank --type=angular --no-git --no-link <<< $'n'" && \
    cd /opt/notepad/ionic-notepad && \
    ionic g page Detail && \
    ionic g service services/Notes && \
    npm install @ionic/storage --save && \
    mkdir -p /opt/notepad/ionic-notepad/src/app/interfaces/


COPY ["./overlay/src/app/app-routing.module.ts", "overlay/src/app/app.module.ts", "/opt/notepad/ionic-notepad/src/app/"]
COPY ["./overlay/src/app/home/*", "/opt/notepad/ionic-notepad/src/app/home/"]
COPY ["./overlay/src/app/detail/*", "/opt/notepad/ionic-notepad/src/app/detail/"]
COPY ["./overlay/src/app/services/*", "/opt/notepad/ionic-notepad/src/app/services/"]
COPY ["./overlay/src/app/interfaces/*", "/opt/notepad/ionic-notepad/src/app/interfaces/"]
COPY ["./overlay/src/theme/*", "/opt/notepad/ionic-notepad/src/app/theme/"]

CMD cd /opt/notepad/ionic-notepad && ionic serve
