ionic_notepad
=============

A simple ionic notepad taken from a tutorial by Josh Morony
https://www.joshmorony.com/building-a-notepad-application-from-scratch-with-ionic/

Run: docker run -t -p 8100:8100 ionic_notepad
